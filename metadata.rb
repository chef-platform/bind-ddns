# frozen_string_literal: true

name 'bind-ddns'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/bind-ddns@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and configure ISC Bind on server and nsupdate on clients'
long_description File.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/bind-ddns'
issues_url 'https://gitlab.com/chef-platform/bind-ddns/issues'
version '1.14.0'

chef_version '>= 12.0'

supports 'centos', '>= 7.1'
